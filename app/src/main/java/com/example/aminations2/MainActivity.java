package com.example.aminations2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fade(View view){

        Log.i("INFO", "Animating!");


        //ImageView homerImageView = (ImageView) findViewById(R.id.imageView3);
        ImageView bartImageView = (ImageView) findViewById(R.id.imageView2);

        // bartImageView.animate().translationYBy(1000).setDuration(2000);
        // bartImageView.animate().translationXBy(-1000).setDuration(2000);
        // bartImageView.animate().rotation(180).alpha(0).setDuration(3000);
        bartImageView.animate().scaleX(0.5f).scaleY(0.5f).setDuration(1000);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView bartImageView = (ImageView) findViewById(R.id.imageView2);
        bartImageView.setX(-1000);
        bartImageView.animate().translationXBy(1000).rotation(1800).setDuration(3000);


    }
}
